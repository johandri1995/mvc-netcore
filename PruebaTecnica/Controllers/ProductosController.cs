﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PruebaTecnica.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class ProductosController : Controller
    {

        private readonly dbproductosContext _context;

        public ProductosController(dbproductosContext context)
        {
            _context = context;
        }
        // GET: ProductosController
        public ActionResult Index()
        {


            var query = from p in _context.Producto
                        join c in _context.Categoria on p.Idcategoria equals c.Idcategoria
                        select new ProductosCategoria
                        {
                            Idproducto = p.Idproducto,
                            Categoria = c.Nombre,
                            Precio_venta = p.Precio_venta,
                            Codigo = p.Codigo,
                            Estado = p.Estado,
                            Stock = p.Stock,
                            Descripcion = p.Descripcion,
                            Nombre = p.Nombre,
                        };


            IEnumerable<ProductosCategoria> listaProductos = query;
            return View(listaProductos);
        }

        // GET: ProductosController/Details/5
        public ActionResult Details(int id)
        {
            var producto = _context.Producto.Find(id);

            ViewBag.imgBase64 = null;

            if (producto.Imagen != null)
                ViewBag.imgBase64 = Convert.ToBase64String(producto.Imagen);

            return View(producto);
        }

        // GET: ProductosController/Create
        public ActionResult Create()
        {

            List<SelectListItem> listItems = new List<SelectListItem>();

            var categorias = _context.Categoria;

            ViewBag.Idcategoria = new SelectList(categorias.Select(i => new SelectListItem()
            {
                Text = Convert.ToString(i.Nombre),
                Value = Convert.ToString(i.Idcategoria)
            }).ToList(), "Value", "Text");


            return View();
        }

        // POST: ProductosController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {

                    string ruta;
                    var archivo = collection.Files["Imagen"];
                    byte[] imageArray = null;

                    if (archivo != null)
                    {
                        ruta = Path.GetFullPath("wwwroot\\Imagenes\\" + archivo.FileName);

                        using (var stream = new FileStream(ruta, FileMode.Create))
                        {
                            archivo.CopyTo(stream);
                        }
                        imageArray = System.IO.File.ReadAllBytes(ruta);
                    }

                    

                    Producto pr = new Producto();
                    pr.Idproducto = Convert.ToInt32(collection["Idproducto"]);
                    pr.Idcategoria = Convert.ToInt32(collection["Idcategoria"]);
                    pr.Codigo = collection["Codigo"].ToString();
                    pr.Nombre = collection["Nombre"].ToString();
                    pr.Precio_venta = Convert.ToDecimal(collection["Precio_venta"]);
                    pr.Stock = Convert.ToInt32(collection["Stock"]);
                    pr.Descripcion = collection["Descripcion"].ToString();
                    if (imageArray != null)
                        pr.Imagen = imageArray;
                    pr.Estado = true;//Convert.ToBoolean(collection["Estado"]);

            
                    TempData["mensaje"] = "Producto Creado Correctamente";
                    _context.Producto.Add(pr);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception e)
            {
                return View(e);
            }
        }

        // GET: ProductosController/Edit/5
        public ActionResult Edit(int id)
        {
            var producto = _context.Producto.Find(id);

            List<SelectListItem> listItems = new List<SelectListItem>();

            var categorias = _context.Categoria;

            ViewBag.Idcategoria = new SelectList(categorias.Select(i => new SelectListItem()
            {
                Text = Convert.ToString(i.Nombre),
                Value = Convert.ToString(i.Idcategoria)
            }).ToList(), "Value", "Text");


            ViewBag.imgBase64 = null;

            if (producto.Imagen != null)
                ViewBag.imgBase64 = Convert.ToBase64String(producto.Imagen);

            return View(producto);
        }

        // POST: ProductosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {
                    string ruta;
                    var archivo = collection.Files["Imagen"];
                    byte[] imageArray = null;

                    if (archivo != null)
                    {
                        ruta = Path.GetFullPath("wwwroot\\Imagenes\\" + archivo.FileName);

                        using (var stream = new FileStream(ruta, FileMode.Create))
                        {
                            archivo.CopyTo(stream);
                        }
                        imageArray = System.IO.File.ReadAllBytes(ruta);
                    }

                    Producto pr = new Producto();
                    pr.Idproducto = Convert.ToInt32(collection["Idproducto"]);
                    pr.Idcategoria = Convert.ToInt32(collection["Idcategoria"]);
                    pr.Codigo = collection["Codigo"].ToString();
                    pr.Nombre = collection["Nombre"].ToString();
                    pr.Precio_venta = Convert.ToDecimal(collection["Precio_venta"]);
                    pr.Stock = Convert.ToInt32(collection["Stock"]);
                    pr.Descripcion = collection["Descripcion"].ToString();
                    if(imageArray !=null)
                        pr.Imagen =  imageArray;
                    pr.Estado = true;//Convert.ToBoolean(collection["Estado"]);

                    _ = pr;
                    TempData["mensaje"] = "Producto Editado Correctamente";
                    _context.Producto.Update(pr);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception e)
            {
                return View(e);
            }
        }

        // GET: ProductosController/Delete/5
        public ActionResult Delete(int id)
        {
            var producto = _context.Producto.Find(id);            
            return View(producto);
        }

        // POST: ProductosController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                Producto pro = _context.Producto.Find(id);

                TempData["mensaje"] = "Producto eliminado Correctamente";
                _context.Producto.Remove(pro);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));

            }
            catch (Exception e)
            {
                return View(e);
            }
        }
    }
}

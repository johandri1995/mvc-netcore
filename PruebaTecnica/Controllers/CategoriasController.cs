﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PruebaTecnica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class CategoriasController : Controller
    {
        private readonly dbproductosContext _context;

        public CategoriasController(dbproductosContext context)
        {
            _context = context;
        }

        // GET: CategiriaController
        public ActionResult Index()
        {

            IEnumerable<Categoria> listaCategorias = _context.Categoria;

            return View(listaCategorias);
        }

        // GET: CategiriaController/Details/5
        public ActionResult Details(int id)
        {

            var categoria = _context.Categoria.Find(id);
            return View(categoria);
        }

        // GET: CategiriaController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategiriaController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Categoria categoria)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {

                    TempData["mensaje"] = "Categoria Creada Correctamente";
                    _context.Categoria.Add(categoria);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception e)
            {
                return View(e);
            }
        }

        // GET: CategiriaController/Edit/5
        public ActionResult Edit(int id)
        {
            var categoria = _context.Categoria.Find(id);
            return View(categoria);
        }

        // POST: CategiriaController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Categoria categoria)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {

                    TempData["mensaje"] = "Categoria Editada Correctamente";
                    _context.Categoria.Update(categoria);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception e)
            {
                return View(e);
            }
        }

        // GET: CategiriaController/Delete/5
        public ActionResult Delete(int id)
        {
            var categoria = _context.Categoria.Find(id);
            return View(categoria);
        }

        // POST: CategiriaController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                Categoria cat = _context.Categoria.Find(id);

                TempData["mensaje"] = "Categoria eliminada Correctamente";
                _context.Categoria.Remove(cat);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));

            }
            catch (Exception e)
            {
                return View(e);
            }
        }
    }
}

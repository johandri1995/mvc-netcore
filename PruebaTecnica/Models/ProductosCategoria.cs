﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Models
{
    public class ProductosCategoria
    {

        public int Idproducto { get; set; }

        public string Categoria { get; set; }

        public string Codigo { get; set; }

        public string Nombre { get; set; }

        public decimal Precio_venta { get; set; }

        public int Stock { get; set; }

        public string Descripcion { get; set; }

        public string Imagen { get; set; }

        public bool Estado { get; set; }
    }
}

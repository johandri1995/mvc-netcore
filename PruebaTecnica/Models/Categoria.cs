﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace PruebaTecnica.Models
{
    public partial class Categoria
    {
        public Categoria()
        {
            Productos = new HashSet<Producto>();
        }

        [KeyAttribute]
        public int Idcategoria { get; set; }

        [Required(ErrorMessage = "Nombre es obligatorio")]
        [StringLength(50, ErrorMessage = "El nombre no debe ser mayor a 50 caracteres")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "La Descripcion es obligatoria")]
        [StringLength(50, ErrorMessage = "El nombre no debe ser mayor a 50 caracteres")]
        public string Descripcion { get; set; }

        [Display(Name = "Activo")]
        public bool Estado { get; set; }

        public virtual ICollection<Producto> Productos { get; set; }

        
    }
}

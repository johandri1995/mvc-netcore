﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

#nullable disable

namespace PruebaTecnica.Models
{
    public partial class Producto
    {
        [KeyAttribute]
        public int Idproducto { get; set; }

        [Required(ErrorMessage = "Nombre es obligatorio")]
        [Display(Name = "Categoria")]
        public int Idcategoria { get; set; }

        [Required(ErrorMessage = "Nombre es obligatorio")]
        [StringLength(64, ErrorMessage = "La codigo no debe ser mayor a 64 caracteres")]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "Nombre es obligatorio")]
        public string Nombre { get; set; }

        [Display(Name = "Precio de Venta")]
        [Required(ErrorMessage = "Precio de venta es obligatorio")]
        public decimal Precio_venta { get; set; }

        [Required(ErrorMessage = "Stock Requerido")]
        public int Stock { get; set; }

        [StringLength(255, ErrorMessage = "La descripción no debe ser mayor a 50 caracteres")]
        public string Descripcion { get; set; }

        [StringLength(255, ErrorMessage = "La imagen no debe ser mayor a 50 caracteres")]
        public byte[] Imagen { get; set; }

        [Display(Name = "Activo")]
        public bool Estado { get; set; }

        public virtual Categoria IdcategoriaNavigation { get; set; }
    }
}
